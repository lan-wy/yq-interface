/**
 * 定义一个Ajax请求的配置接口
 * @interface AjaxOptions
 * @property {string} url - 请求的URL地址
 * @property {string} baseURL - 请求的baseURL地址
 * @property {string} [method] - 请求的方法，默认为GET，可选值包括GET, POST, PUT, DELETE等
 * @property {string} [ContentType] - 请求的内容类型，例如 'application/json' 或 'application/x-www-form-urlencoded'
 * @property {any} [params] - 请求的参数，可以是任意类型的对象，通常用于拼接在URL后
 * @property {any} [data] - 请求发送的数据，可以是任意类型的对象
 * @property {number} [timeout] - 请求的超时时间，单位为毫秒
 * @property {boolean} [withCredentials] - 是否携带跨域请求的凭证（cookies）
 * @property {'json' | 'text' | 'blob' | 'arraybuffer' | 'document' | 'stream'} [ResponseType] - 期望的响应类型
 * @property {boolean} [isLoading] - 标记请求是否正在加载
 * @property {any} [loadingConfig] - 加载状态的配置项，可以是任意类型的对象
 * @property {(data: any) => any} [resultFilter] - 请求返回数据的过滤器函数，可用于数据处理和转换
 */
export interface AjaxOptions {
    Headers: any;
    url: string;
    baseURL: string;
    method?: string;
    ContentType?: string;
    params?: any;
    data?: any;
    timeout?: number;
    withCredentials?: boolean;
    ResponseType?: 'json' | 'text' | 'blob' | 'arraybuffer' | 'document' | 'stream';
    isLoading?: boolean;
    loadingConfig?: any;
    resultFilter?: (data: any) => any;
}
/**
 * 定义一个Ajax请求的配置接口
 * 该接口用于配置Ajax请求的各种行为，包括加载状态切换、请求锁定、请求完成、成功回调、错误回调和取消回调等
 */
export interface AjaxConfig {
    /**
     * 加载状态切换的回调函数
     * @param value boolean类型，表示加载状态是否开启
     * 该函数用于在请求开始和结束时切换页面的加载状态（如显示或隐藏加载动画）
     */
    loadingSwitch?: (value: boolean) => void;
    /**
     * 请求锁定的函数
     * @returns boolean类型，表示是否允许继续发送请求
     * 该函数用于在请求发送前进行一些检查，如防止重复发送请求等
     */
    lock?: () => boolean;
    /**
     * 请求完成的回调函数
     * @param url string类型，表示完成请求的URL
     * 该函数在请求无论成功或失败，只要完成就会被调用，可用于记录日志等操作
     */
    complete?: (url: string) => void;
    /**
     * 请求成功时的回调函数
     * @param data any类型，表示请求成功返回的数据
     * 该函数用于处理请求成功后需要执行的操作，如将数据渲染到页面等
     */
    callback?: (data: any) => void;
    /**
     * 请求错误时的回调函数
     * @param message any类型，表示错误信息
     * @param status number类型，表示HTTP状态码
     * 该函数用于处理请求错误时需要执行的操作，如显示错误信息等
     */
    errorCallback?: (message: any, status: number) => void;
    /**
     * 请求取消时的回调函数
     * @param err any类型，表示取消请求时的错误信息
     * 该函数用于处理请求取消时需要执行的操作，如清理资源等
     */
    cancelCallback?: (err: any) => void;
}
/**
 * 定义默认的HTTP请求头。
 *
 * 这是一个对象，键是请求头的字段名，值是字段的值。
 * 主要用于设置跨域请求的Access-Control-Allow-Origin以及标识请求类型为XMLHttpRequest。
 */
export declare const defaultHeaders: {
    'Access-Control-Allow-Origin': string;
    'X-Requested-With': string;
};
/**
 * 获取完整URL
 *
 * @param options 配置选项
 * @param options.baseURL 基础URL
 * @param options.url 路径或相对URL
 * @returns 拼接后的完整URL
 */
export declare function getFullUrl(options: AjaxOptions): string;
