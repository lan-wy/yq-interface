import { AjaxOptions, AjaxConfig } from './config';
declare function ImplementAxios(options: AjaxOptions, ajaxConfig: AjaxConfig): Promise<void>;
export default ImplementAxios;
