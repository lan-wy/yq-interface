/**
 * @Author: WangY
 * @Description: AXIOS 封装的方法TS版
 */
import { AjaxConfig } from './config';
declare function interfaceAxios(options: any, ajaxConfig?: AjaxConfig): any;
export default interfaceAxios;
