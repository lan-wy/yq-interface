interface HttpCodeMessage {
    message: string;
    status?: number;
}
export default function httpCodeFilter(code: number): HttpCodeMessage;
export {};
