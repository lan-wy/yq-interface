/**
 * 加载动画
 */
type LoadingConfig = {
    text?: string;
    background?: string;
};
type LoadingState = {
    hide: any;
    show: any;
    instance: any;
    count: number;
};
export declare const loading: LoadingState;
export declare const showLoading: (config?: LoadingConfig) => void;
export declare const hideLoading: () => void;
export {};
