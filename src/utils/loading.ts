/*
 * @Author: WangY
 * @Description: API加载loading
 */
import { ElLoading } from 'element-plus';
/**
 * 加载动画
 */

type LoadingConfig = {
	text?: string;
	background?: string;
};

type LoadingState = {
	hide: any;
	show: any;
	instance: any;
	count: number;
};

const createLoading = (): LoadingState => {
	let instance: any = null;
	let count = 0;

	const show = (config?: LoadingConfig) => {
		if (count === 0) {
			instance = ElLoading.service({
				lock: true,
				text: config?.text ?? '拼命加载中...',
				background: config?.background ?? 'rgba(0, 0, 0, 0.7)',
			});
		}
		count++;
	};

	const hide = () => {
		count--;
		if (instance && count <= 0) {
			instance.close();
			instance = null;
		}
	};

	return {
		instance,
		count,
		show,
		hide,
	};
};

export const loading = createLoading();

export const showLoading = (config?: LoadingConfig) => {
	loading.show(config);
};

export const hideLoading = () => {
	loading.hide();
};
