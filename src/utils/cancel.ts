/*
 * @Author: WangY
 * @Description: 取消重复的请求
 */
import axios from 'axios';

// 函数返回唯一的请求key
function getRequestKey(config: any) {
	const { method, url, params, data } = config;
	return [method, url, JSON.stringify(params), JSON.stringify(data)].join('&');
}

// 添加请求信息
const pendingRequest = new Map();

export function addPendingRequest(config: any) {
	const requestKey = getRequestKey(config);
	config.cancelToken =
		config.cancelToken ||
		new axios.CancelToken((cancel) => {
			if (!pendingRequest.has(requestKey)) {
				pendingRequest.set(requestKey, cancel);
			}
		});
}

// 取消重复请求，移除重复请求信息
export function removePendingRequest(config: any) {
	const requestKey = getRequestKey(config);
	if (pendingRequest.has(requestKey)) {
		// 如果是重复的请求，则执行对应的cancel函数
		const cancel = pendingRequest.get(requestKey);
		cancel(requestKey);
		// 将前一次重复的请求移除
		pendingRequest.delete(requestKey);
	}
}
