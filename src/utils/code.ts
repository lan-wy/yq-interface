/*
 * @Author: WangY
 * @Description: 常见http 状态码过滤
 */

interface HttpCodeMessage {
	message: string;
	status?: number;
}

const httpCodeMessages: { [code: number]: HttpCodeMessage } = {
	400: { message: '错误请求', status: 400 },
	401: { message: '未授权', status: 401 },
	403: { message: '拒绝访问', status: 403 },
	404: { message: '请求错误,未找到该资源', status: 404 },
	405: { message: '请求方法未允许', status: 405 },
	408: { message: '请求超时', status: 408 },
	500: { message: '服务器端出错', status: 500 },
	501: { message: '网络未实现', status: 501 },
	502: { message: '网络错误', status: 502 },
	503: { message: '服务不可用', status: 503 },
	504: { message: '网络超时', status: 504 },
	505: { message: 'http版本不支持该请求', status: 505 },
};

export default function httpCodeFilter(code: number): HttpCodeMessage {
	return httpCodeMessages[code] || { message: '网络连接错误' };
}
