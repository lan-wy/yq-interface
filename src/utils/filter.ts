/*
 * @Author: WangY
 * @Description: 入参过滤方法
 */

const paramsFilter = (params: any, ContentType?: string): any => {
  if (!params) return null;

  if (!ContentType || !ContentType.includes('multipart/form-data')) {
    // 创建过滤后的对象，只包含非null和非undefined的值
    const res: { [key: string]: any } = {};
    // 遍历params对象的每个键值对
    Object.entries(params).forEach(([key, value]: [string, any]) => {
      if (value !== null && value !== undefined) {
        res[key] = value;
      }
    });
    return res;
  } else {
    // 创建一个新的FormData实例用于处理multipart/form-data格式的数据
    const formData = new FormData();
    // 遍历params对象的每个键值对
    Object.entries(params).forEach(([key, value]: [string, any]) => {
      if (key === 'file') {
        // 如果键是'file'，则假定它的值是一个数组，并遍历数组将其内容添加到formData中
        for (let i = 0; i < value.length; i++) {
          formData.append(key, value[i]);
        }
      } else {
        // 对于非'file'的键，直接将其添加到formData中
        formData.append(key, value);
      }
    });
    return formData;
  }
};

export default paramsFilter;
