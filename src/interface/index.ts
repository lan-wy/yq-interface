/**
 * @Author: WangY
 * @Description: AXIOS 封装的方法TS版
 */

import { ElMessage } from 'element-plus';
import ImplementAxios from './interface';
import { AjaxConfig } from './config';

// axios 调用方法
function interfaceAxios(options: any, ajaxConfig: AjaxConfig = {}) {
	// 默认配置
	const defaultConfig: AjaxConfig = {
		// 默认处理完成
		complete: (url: string) => {
			console.log('接口调用！接口调用！： ===>> ' + '【 ' + import.meta.env.VITE_API_URL + url + ' 】');
		},
		// 默认处理成功
		callback: (data: any) => {
			console.log('调用成功！数据返回！： ===>> ' + data);
		},
		// 取消请求
		cancelCallback: (err: any) => {
			console.log('请求已被取消！： ===>> ' + err.message);
		},
		// 默认处理失败
		errorCallback: (message: any, status: number) => {
			console.log('接口报错！接口报错！： ===>> ' + '【 ' + 'message ===>> ' + message + '/' + ' status ===>> ' + status + ' 】');
			ElMessage.error(message);
		},
	};
	const config: AjaxConfig = { ...defaultConfig, ...ajaxConfig };
	const agent: any = {};
	// 成功 ===>> 执行
	agent.then = (callback: any) => {
		if (callback) {
			config.callback = (data: any) => {
				callback(data);
			};
		}
		return agent;
	};
	// 失败 ===>> 执行
	agent.catch = (errorCallback: any) => {
		if (errorCallback) {
			config.errorCallback = (message: any, status: number) => {
				errorCallback(message, status);
			};
		}
		return agent;
	};
	// 都会 ===>> 执行
	agent.finally = (complete: any) => {
		if (complete) {
			config.complete = (url: any) => {
				complete(url);
			};
		}
		return agent;
	};
	// 请求时的加载loading 【 button 】
	agent.loadingSwitch = (target: any, key: any) => {
		config.loadingSwitch = (value: any) => {
			target[key] = value;
		};
		return agent;
	};
	// 通过调用lockSwitch方法，可以设置一个锁定状态，当锁定状态为true时，请求不会发起
	agent.lockSwitch = (target: any, key: any) => {
		config.lock = () => {
			return target[key];
		};
		ajaxConfig.loadingSwitch = (value: any) => {
			target[key] = value;
		};
		return agent;
	};
	setTimeout(() => {
		if (config.lock && config.lock()) return;
		config.loadingSwitch && config.loadingSwitch(true);
		ImplementAxios(options, config);
	}, 1);
	return agent;
}

export default interfaceAxios;
