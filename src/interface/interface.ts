/*
 * @Author: WangY
 * @Description: axios 执行方法
 */
import axios from 'axios';
import httpCodeFilter from '../utils/code';
import paramsFilter from '../utils/filter';
import { showLoading, hideLoading } from '../utils/loading';
import { addPendingRequest, removePendingRequest } from '../utils/cancel';
import { ElMessage } from 'element-plus';
import { AjaxOptions, AjaxConfig, defaultHeaders, getFullUrl } from './config';

// 请求成功处理逻辑
function handleSuccess(res: any, options: AjaxOptions, ajaxConfig: AjaxConfig) {
	if (res.data.code == 200) {
		ajaxConfig.callback && ajaxConfig.callback(options.resultFilter ? options.resultFilter(res.data) : res.data);
	} else {
		ajaxConfig.errorCallback && ajaxConfig.errorCallback(res.data.errorMsg || '请求失败，请联系管理员', res.data.code);
	}
}

// 请求失败处理逻辑
function handleError(error: any, ajaxConfig: AjaxConfig) {
	const TuneParameters: any = httpCodeFilter(error.response.status);
	ajaxConfig.errorCallback && ajaxConfig.errorCallback(TuneParameters.message, TuneParameters.status);
}

// axios 执行方法
async function ImplementAxios(options: AjaxOptions, ajaxConfig: AjaxConfig) {
	if (options.isLoading) showLoading(options.loadingConfig);
	try {
		const config = {
			url: getFullUrl(options),
			method: options.method || 'post',
			headers: {
				...defaultHeaders,
				'Content-Type': options.ContentType ? options.ContentType : 'application/json;charset=UTF-8',
				// authorization: null,
				// token: null,
				...options.Headers,
			},
			params: paramsFilter(options.params, options.ContentType),
			data: paramsFilter(options.data, options.ContentType),
			timeout: options.timeout || 10000,
			withCredentials: options.withCredentials || false,
			responseType: options.ResponseType ? options.ResponseType : 'json',
		};
		removePendingRequest(config);
		addPendingRequest(config);
		const res = await axios(config);
		// console.log(res)
		removePendingRequest(res.config);
		if (res.status == 200) {
			// console.log(res);
			handleSuccess(res, options, ajaxConfig);
		} else {
			handleError(res, ajaxConfig);
		}
	} catch (error: any) {
		removePendingRequest(error?.config || {});
		if (axios.isCancel(error)) {
			// 判断error是否为axios.CancelToken抛出的
			ajaxConfig.cancelCallback && ajaxConfig.cancelCallback(error);
		} else {
			if (error.message.indexOf('timeout') != -1) {
				ElMessage.error('网络超时');
			} else if (error.message == 'Network Error') {
				ElMessage.error('网络连接错误');
			} else {
				if (error.response.data) ElMessage.error(error.response.statusText);
				else handleError(error, ajaxConfig);
			}
		}
	} finally {
		hideLoading();
		ajaxConfig.loadingSwitch && ajaxConfig.loadingSwitch(false);
		ajaxConfig.complete && ajaxConfig.complete(options.url);
	}
}
export default ImplementAxios;
